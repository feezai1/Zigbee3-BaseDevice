/*****************************************************************************
 *
 * MODULE:             JN-AN-1217
 *
 * COMPONENT:          app_led_interface.c
 *
 * DESCRIPTION:        DK4 DR1175 Led interface (White Led)
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5168, JN5179].
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2016. All rights reserved
 *
 ***************************************************************************/

#include <jendefs.h>

#include <AppHardwareApi_JN516x.h>
#include "dbg.h"

#define ROUTER_LIGHT_CHAN2_LED    (1<<2)   /* using DIO2 */
#define ROUTER_LIGHT_CHAN1_LED   (1<<3)   /* using DIO3 */


PUBLIC void APP_vLedInitialise(void)
{
    vAHI_DioSetPullup(0, ROUTER_LIGHT_CHAN1_LED);
    vAHI_DioSetPullup(0, ROUTER_LIGHT_CHAN2_LED);

    /*Make the DIo as output*/
    vAHI_DioSetDirection(0, ROUTER_LIGHT_CHAN1_LED);
    vAHI_DioSetDirection(0, ROUTER_LIGHT_CHAN2_LED);
}


PUBLIC void APP_vSetLed(uint8 chan, bool_t bOn)
{
    if (chan == 0) {
        if (bOn) {
            vAHI_DioSetOutput(ROUTER_LIGHT_CHAN1_LED, 0);
            vAHI_DioSetOutput(ROUTER_LIGHT_CHAN2_LED, 0);
        } else {
            vAHI_DioSetOutput(0, ROUTER_LIGHT_CHAN1_LED);
            vAHI_DioSetOutput(0, ROUTER_LIGHT_CHAN2_LED);
        }
        return;
    }

    if (chan == 1) {
        if (bOn) {
            vAHI_DioSetOutput(ROUTER_LIGHT_CHAN1_LED, 0);
        } else {
            vAHI_DioSetOutput(0, ROUTER_LIGHT_CHAN1_LED);
        }
        return;
    }

    if (chan == 2) {
        if (bOn) {
            vAHI_DioSetOutput(ROUTER_LIGHT_CHAN2_LED, 0);
        } else {
            vAHI_DioSetOutput(0, ROUTER_LIGHT_CHAN2_LED);
        }
    }
}


















